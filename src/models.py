from tortoise import fields
from tortoise.models import Model


class User(Model):
    username = fields.CharField(max_length=255, unique=True)
    password_hash = fields.CharField(max_length=255)


class Post(Model):
    user = fields.ForeignKeyField("models.User", related_name="posts")
    content = fields.TextField()
    likes = fields.ManyToManyField("models.User", related_name="liked_posts")


async def is_post_owner(user_id: int, post_id: int) -> bool:
    post = await Post.filter(id=post_id).first().prefetch_related("user")
    return post.user_id == user_id
