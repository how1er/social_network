from fastapi import APIRouter, Depends, HTTPException, status
from tortoise.contrib.fastapi import HTTPNotFoundError

from auth import create_access_token, get_current_user, verify_password, get_password_hash
from models import User, Post, is_post_owner
from schemas import UserCreate, UserOut , PostCreate, PostOut


router = APIRouter()


@router.post("/signup", response_model=UserOut)
async def signup(user: UserCreate):
    hashed_password = get_password_hash(user.password)
    created_user = User(username=user.username, password_hash=hashed_password)
    await created_user.save()
    return created_user


@router.post("/login")
async def login(username: str, password: str):
    user = await User.filter(username=username).first()
    if not user or not verify_password(password, user.password_hash):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token = create_access_token(user.id)
    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/posts", response_model=PostOut)
async def create_post(post: PostCreate, current_user: User = Depends(get_current_user)):
    created_post = await Post.create(user=current_user, content=post.content)
    return created_post


@router.put("/posts/{post_id}", response_model=PostOut, dependencies=[Depends(is_post_owner)])
async def update_post(post_id: int, post: PostCreate):
    updated_post = await Post.filter(id=post_id).update(content=post.content)
    if updated_post == 1:
        return await Post.filter(id=post_id).first()
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Post not found")


@router.delete("/posts/{post_id}", response_model=None, dependencies=[Depends(is_post_owner)])
async def delete_post(post_id: int):
    deleted_count = await Post.filter(id=post_id).delete()
    if deleted_count == 1:
        return {"message": "Post deleted successfully"}
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Post not found")


@router.get("/posts/{post_id}", response_model=PostOut, responses={404: {"model": HTTPNotFoundError}})
async def get_post(post_id: int):
    return await Post.filter(id=post_id).first()


@router.post("/posts/{post_id}/like")
async def like_post(post_id: int, current_user: User = Depends(get_current_user)):
    post = await Post.filter(id=post_id).first().prefetch_related("user", "likes")
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Post not found")

    if post.user_id == current_user.id:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You cannot like your own post",
        )

    if current_user in post.likes:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You have already liked this post",
        )

    await post.likes.add(current_user)
    post.likes_count = len(post.likes)
    await post.save()
    return post


@router.post("/posts/{post_id}/dislike")
async def dislike_post(post_id: int, current_user: User = Depends(get_current_user)):
    post = await Post.filter(id=post_id).first().prefetch_related("likes")
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Post not found")

    if current_user in post.likes:
        await post.likes.remove(current_user)
        post.likes_count = len(post.likes)
        await post.save()

    return post
