from pydantic import BaseModel
from models import User


class UserCreate(BaseModel):
    username: str
    password: str


class UserOut(BaseModel):
    id: int
    username: str


class PostCreate(BaseModel):
    content: str


class PostOut(BaseModel):
    id: int
    user_id: int
    content: str
