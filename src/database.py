from tortoise import Tortoise


async def init_db():
    await Tortoise.init(
        db_url='sqlite://./database.db',
        modules={'models': ['models']},
    )
    await Tortoise.generate_schemas()


async def close_db():
    await Tortoise.close_connections()