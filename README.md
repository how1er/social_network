# Social Network

## Installation

Clone the repository to your computer:
```bash
git clone https://gitlab.com/how1er/social_network.git
```
## Usage

Build the container using Docker Compose:
```bash
docker-compose up --build
```
To see the documentation

```bash
http://127.0.0.1:8000/docs
```
